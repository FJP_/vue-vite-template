/**
 * 将对象中的类型设置成指定类型
 */
type setType<T, U> = {
  [K in keyof T]: U;
};

/**
 * 将类型全部变为可选
 */
type Partial<T> = {
  [K in keyof T]?: T[K];
};
/**
 *将类型全部变为可选（嵌套）
 */
type DeepPartical<T> = {
  // eslint-disable-next-line @typescript-eslint/ban-types
  [K in keyof T]?: T[K] extends object ? DeepPartical<T[K]> : T[K];
};
/**
 * 挑选键值后的类型
 */
type Pick<T, K extends keyof T> = {
  K: T[K];
};
type Exclude<T, K> = T extends K ? never : T;
/**
 * 获取去除键值后的类型
 */
type Remove<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/**
 * 获取返回值类型
 */
type ReturnType<T> = T extends (...args: any) => infer R ? R : any;
/**
 *过滤可选属性
 */
type Required<T> = {
  // eslint-disable-next-line @typescript-eslint/ban-types
  [K in keyof T]: {} extends Pick<T, K> ? never : K;
}[keyof T];
/**
 * 提交函数类型定义
 */
type Submit = (query: { [key: string]: any }) => void;

export type { Partial, Pick, Remove, ReturnType, Required, setType,Submit};




