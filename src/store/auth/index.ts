import { defineStore } from "pinia";
import { authStateType } from "@/store/auth/type";

const authStore = defineStore("auth", {
  state: (): authStateType => ({
    token: "",
  }),
  actions: {
    setToken(token: string): void {
      this.token = token;
    },
    reRegister() {
      this.token = "";
    },
  },
  persist: {
    enabled: true,
    strategies: [
      {
        storage: localStorage,
        paths: ["token"],
      },
    ],
  },
});

export default authStore;
