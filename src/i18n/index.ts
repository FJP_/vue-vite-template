import enLocale from './en.json';
import cnLocale from './cn.json';
import frLocale from './fr.json';
import {createI18n} from "vue-i18n";

const messages:any = {
  en: {
    ...enLocale,
  },
  fr: {
    ...frLocale,
  },
  cn: {
    ...cnLocale,
  },
};

const getCookie = (name:string):string => {
  const result:Array<string>|null = document.cookie.match(`(^|[^;]+)\\s*${name}\\s*=\\s*([^;]+)`);
  return result ? (result.pop()) as string : '';
};

const getLocale = ():string => {
  if (getCookie('language')) return getCookie('language');
  const language:string = navigator.language.toLowerCase();
  const result = ['en', 'fr', 'cn'].filter((value) => language.includes(value));
  return result ? result[0] : 'en';
};

const i18n = createI18n({
  locale: getLocale(),
  messages,
  silentTranslationWarn: true,
});

export default i18n;
