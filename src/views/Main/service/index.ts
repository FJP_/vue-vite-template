import type { Submit } from "@/type";
import { service } from "@/service";
import authStore from "@/store/auth";
import type { menuItem } from "@/views/Main/components/SIdeBar/type";
import type { Ref } from "vue";

const loginIn: Submit = async (query) => {
  const store = authStore();
  try {
    const {
      data: { token },
    } = await service.post("/auth/login", query);
    store.setToken(token);
  } catch (e) {
    console.log(e);
  }
};

const getMenu = async (menuData: Ref<Array<menuItem>>) => {
  try {
    // eslint-disable-next-line no-param-reassign
    ({ data: menuData.value } = await service.get("/resources/menu/me"));
  } catch (e) {
    console.log(e);
  }
};

export { loginIn, getMenu };
