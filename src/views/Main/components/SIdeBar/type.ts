interface menuItem {
  id: number;
  cn: string;
  en: string;
  fr: string;
  name: string;
  children: Array<menuItem>;
  [key: string]: any;
}

export type { menuItem };
