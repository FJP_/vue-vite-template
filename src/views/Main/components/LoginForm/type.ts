interface userDataType {
  username: string;
  password: string;
  appKey: string;
}

export type { userDataType };
