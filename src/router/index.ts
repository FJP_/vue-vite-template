import type { Router, RouteRecordRaw ,NavigationGuard} from "vue-router";
import { createRouter, createWebHistory } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/home",
    name: "home",
    component: () => import("@/App.vue"),
  },
];
const router: Router = createRouter({
  history: createWebHistory("/"),
  routes,
});

export default router;
