// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import QS from "qs";
import axios from "axios";
import authStore from "@/store/auth";

const service = axios.create({
  baseURL: "/api",
  paramsSerializer: (params) => QS.stringify(params, { arrayFormat: "brackets" }),
});

service.interceptors.request.use((config) => {
  const auth = authStore();
  if (auth.token) {
    config.headers!.Authorization = `Bearer ${auth.token}`;
  }
  return config;
});

service.interceptors.response.use(
  (res) => res,
  (e) => {
    const auth = authStore();
    if (e.message.includes("401")) {
      auth.reRegister();
    }
    return Promise.reject(e);
  }
);

export { service };
